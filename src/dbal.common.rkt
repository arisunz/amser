#lang racket

(require (prefix-in db: db)
         (prefix-in res: "common.resources.rkt"))

(provide connection-acquire
         with-connection
         db-initialize)

(define db-path (res:get-db-path))

(define connection-pool
  (db:connection-pool
   (λ ()
     (db:sqlite3-connect #:database db-path
                         #:mode 'create))))

(define (connection-acquire)
  (db:connection-pool-lease connection-pool))

(define-syntax with-connection
  (syntax-rules ()
    ((with-connection conn body ...)
     (let ((conn (connection-acquire)))
       (begin0
           (begin body ...)
         (db:disconnect conn))))))

(define tables-sql
  '("CREATE TABLE IF NOT EXISTS appointments (appointment_id INT PRIMARY KEY NOT NULL, date INT, client_id INT, service_id INT, paid_amount INT, total_amount INT, notes TEXT);"
    "CREATE TABLE IF NOT EXISTS clients (client_id INT PRIMARY KEY NOT NULL, name TEXT, phone_number TEXT);"
    "CREATE TABLE IF NOT EXISTS services (service_id INT PRIMARY KEY NOT NULL, service TEXT, professional_name TEXT);"))

(define (db-initialize)
  (for-each (λ (table) (db:query-exec (connection-acquire) table))
            tables-sql))
