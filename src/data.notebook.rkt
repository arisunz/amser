#lang racket

(require (prefix-in apt: "data.appointment.rkt")
         (prefix-in cln: "data.client.rkt")
         (prefix-in srv: "data.service.rkt"))

(provide make-notebook
         add-entry!
         delete-entry!
         get-appointments
         get-clients
         get-services)

(define sections '(clients services appointments))

(define (make-notebook)
  (let ((nb (make-hasheq)))
    (for-each (λ (section)
                (hash-set! nb section (make-hasheqv)))
              sections)
    nb))

(define (get-section entry)
  (cond ((apt:appointment? entry) 'appointments)
        ((cln:client? entry) 'clients)
        ((srv:service? entry) 'services)))

(define (add-entry! notebook id entry)
  (let ((section (get-section entry)))
    (hash-set! (hash-ref notebook section)
               id entry)))

(define (delete-entry! notebook id entry)
  (let ((section (get-section entry)))
    (hash-remove! (hash-ref notebook section)
                  id)))

(define (get-section-as-list notebook section)
  (hash->list (hash-ref notebook section)))


(define (get-appointments notebook)
  (get-section-as-list notebook 'appointments))

(define (get-clients notebook)
  (get-section-as-list notebook 'clients))

(define (get-services notebook)
  (get-section-as-list notebook 'services))
