#lang racket

(require racket/date
         racket/format)

(provide time->date-string
         time->time-string
         destructured-date->time
         past-date?
         today-range)

(define (time->date-string t)
  (parameterize ((date-display-format 'iso-8601))
    (date->string (seconds->date t))))

(define (time->time-string t)
  (let* ((d (seconds->date t))
         (hour (date-hour d))
         (minute (date-minute d)))
    (format "~a:~a"
            (~a hour
                #:pad-string "0"
                #:min-width 2
                #:align 'right)
            (~a minute
                #:pad-string "0"
                #:min-width 2
                #:align 'right))))

(define (destructured-date->time year month day hour minute)
  (date->seconds (make-date 0 minute hour
                            day month year
                            0 0 #f 0)))


(define (past-date? year month day hour minute)
  (< (date->seconds (make-date 0 minute hour
                               day month year
                               0 0 #f 0))
     (current-seconds)))

(define (today-range)
  (let* ((current (seconds->date (current-seconds)))
         (start (date->seconds
                 (make-date 0 0 0
                            (date-day current)
                            (date-month current)
                            (date-year current)
                            0 0 #f 0)))
         (end (date->seconds
               (make-date 59 59 23
                          (date-day current)
                          (date-month current)
                          (date-year current)
                          0 0 #f 0))))
    (values start end)))
