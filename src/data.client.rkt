#lang racket

(require racket/serialize)

(provide client?
         make
         id
         name
         phone-number
         table-values)

(serializable-struct client
  (id name phone-number))

(define make client)
(define id client-id)
(define name client-name)
(define phone-number client-phone-number)

(define (table-values client)
  (values (id client)
          (name client)
          (phone-number client)))
