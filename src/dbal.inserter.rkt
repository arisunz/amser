#lang racket

(require srfi/8/receive
         (prefix-in db: db)
         (prefix-in dbal: "dbal.common.rkt")
         (prefix-in data:appointment: "data.appointment.rkt")
         (prefix-in data:client: "data.client.rkt")
         (prefix-in data:service: "data.service.rkt")
         (prefix-in data:payment: "data.payment.rkt"))

(provide insert)

(define (queries)
  '((appointment . "INSERT INTO appointments VALUES (?, ?, ?, ?, ?, ?, ?)")
    (client . "INSERT INTO clients VALUES (?, ?, ?);")
    (service . "INSERT INTO services VALUES (?, ?, ?);")))

(define (insert-query name)
  (cdr (assq name (queries))))

(define (insert-data . data)
  (dbal:with-connection
   conn
   (apply db:query-exec
          (cons conn data))))

(define (insert-client client)
  (receive (id name phone) (data:client:table-values client)
    (insert-data (insert-query 'client) id name phone))
  client)

(define (insert-service service)
  (let ((new-id (retrieve-new-service-id)))
    (receive (id name prof-name) (data:service:table-values service)
      (insert-data (insert-query 'service) (retrieve-new-service-id) name prof-name))
    (data:service:set-id! service new-id)
    service))

(define (insert-appointment appointment)
  (let ((new-id (retrieve-new-appointment-id)))
    (receive (id date client-id service-id paid total notes)
        (data:appointment:table-values appointment)
      (insert-data (insert-query 'appointment)
                   (retrieve-new-appointment-id)
                   date client-id service-id paid total notes))
    (data:appointment:set-id! appointment new-id)
    appointment))

(define (insert data)
  (cond ((data:appointment:appointment? data) (insert-appointment data))
        ((data:client:client? data) (insert-client data))
        ((data:service:service? data) (insert-service data))
        (else (error "Attempted to insert unsupported data structure. Shame on you."))))

(define (retrieve-new-service-id)
  (dbal:with-connection
   conn
   (let ((val (db:query-maybe-value
               conn
               "SELECT MAX(service_id) FROM services")))
     (if (number? val) (add1 val) 1))))

(define (retrieve-new-appointment-id)
  (dbal:with-connection
   conn
   (let ((val (db:query-maybe-value
               conn
               "SELECT MAX(appointment_id) FROM appointments")))
     (if (number? val) (add1 val) 1))))
