#lang racket

(require (prefix-in db: db)
         (prefix-in dbal: "dbal.common.rkt")
         (prefix-in data:appointment: "data.appointment.rkt")
         (prefix-in data:client: "data.client.rkt")
         (prefix-in data:service: "data.service.rkt")
         (prefix-in data:payment: "data.payment.rkt"))

(provide delete)

(define (queries)
  '((appointment . "DELETE FROM appointments WHERE appointment_id = ?")
    (client . "DELETE FROM clients WHERE client_id = ?")
    (service . "DELETE FROM services WHERE service_id = ?")))

(define (delete-query type)
  (cdr (assq type (queries))))

(define (delete-row type id)
  (dbal:with-connection
   conn
   (db:query-exec conn (delete-query type) id)))

;;;
;;; Deleting either a client or a service will also delete
;;; any appointment associated with it.
;;;
(define (delete-client id)
  (delete-row 'client id)
  (delete-appointment-client-id id))

(define (delete-service id)
  (delete-row 'service id)
  (delete-appointment-service-id id))

(define (delete-appointment id)
  (delete-row 'appointment id))

(define (delete-appointment-client-id id)
  (dbal:with-connection
   conn
   (db:query-exec conn "DELETE FROM appointments WHERE client_id = ?" id)))

(define (delete-appointment-service-id id)
  (dbal:with-connection
   conn
  (db:query-exec conn "DELETE FROM appointments WHERE service_id = ?" id)))

(define (delete data)
  (cond ((data:appointment:appointment? data)
         (delete-appointment (data:appointment:id data)))
        ((data:client:client? data)
         (delete-client (data:client:id data)))
        ((data:service:service? data)
         (delete-service (data:service:id data)))
        (else (error "Attempted to delete unknown structure. Aborting."))))
