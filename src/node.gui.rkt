#lang racket/gui

(require racket/date
         racket/draw
         srfi/8/receive
         "translations.rkt"
         "common.time.rkt"
         "common.appointment-format.rkt"
         (prefix-in msg: "data.message.rkt")
         (prefix-in dis: "data.dispatch.rkt")
         (prefix-in cln: "data.client.rkt")
         (prefix-in srv: "data.service.rkt")
         (prefix-in apt: "data.appointment.rkt")
         (prefix-in pay: "data.payment.rkt"))

(provide set-central-thread!
         update-interface!
         show-main-window
         show-server-disconnect-dialog)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Public interface
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; To send signals
(define central-thread #f)

(define (set-central-thread! th)
  (set! central-thread th))

(define (update-interface! sample data-hash)
  (cond ((cln:client? sample) (update-client-list! (hash-ref data-hash 'clients))
                              (update-choice-hash (hash-ref data-hash 'clients) 'clients)
                              (update-client-choices-ap)
                              (update-client-choices-filter))
        ((srv:service? sample) (update-service-list! (hash-ref data-hash 'services))
                               (update-choice-hash (hash-ref data-hash 'services) 'services)
                               (update-service-choices-ap)
                               (update-service-choices-filter))
        ((or (apt:appointment? sample)
             (eq? 'appointments sample))
         (update-appointment-list! (hash-ref data-hash 'appointments))
         (update-choice-hash (hash-ref data-hash 'appointments) 'appointments))
        (else (void))))

(define (show-main-window)
  (send tlw show #t))

(define (show-server-disconnect-dialog)
  (send server-disconnect-dialog show #t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Private stuff
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define default-filter (λ (x) #t))
(define current-filter default-filter)

(define (restore-filter!)
  (set! current-filter default-filter))

(define (set-filter! f)
  (set! current-filter f))

(define (update-appointment-list! apt-hash)
  (send/apply apts-list set (apts-hash->table apt-hash)))

(define (update-client-list! client-hash)
  (send/apply clients-list set (client-hash->table client-hash)))

(define (update-service-list! service-hash)
  (send/apply serv-list set (service-hash->table service-hash)))


(define client-choices (make-hasheqv))
(define service-choices (make-hasheqv))
(define appointment-hash (make-hasheq))

(define (sort-by-ids data)
  (if (null? data) '()
      (let ((id (cond ((apt:appointment? (car data)) apt:date)
                      ((cln:client? (car data)) cln:id)
                      ((srv:service? (car data)) srv:id)
                      (else (error "Invalid data somehow passed to gui update.")))))
        (letrec ((isort
                  (λ (ls)
                    (sort ls (λ (a1 a2)
                               (< (id a1) (id a2)))))))
          (isort data)))))

(define (appointment-list-by-date (newer-first? #t))
  (let ((apts (map cdr (hash->list appointment-hash)))
        (p? (if newer-first? < >)))
    (sort apts (λ (a1 a2)
                 (p? (apt:date a1) (apt:date a2))))))

(define (hash->sorted-list dhash)
  (sort-by-ids (map cdr (hash->list dhash))))

(define (update-choice-hash dhash which)
  (let ((dlist (if (eq? which 'appointments)
                   (filter current-filter (hash->sorted-list dhash))
                   (hash->sorted-list dhash)))
        (which-hash
         (λ ()
           (cond ((eq? which 'clients) client-choices)
                 ((eq? which 'services) service-choices)
                 ((eq? which 'appointments) appointment-hash)))))
    (hash-clear! (which-hash))
    (for-each (λ (n d) (hash-set! (which-hash) n d))
              (range (length dlist))
              dlist)))

(define (client->choice-item client)
  (format "~a, ~a"
          (cln:name client)
          (cln:id client)))

(define (service->choice-item service)
  (format "~a, ~a"
          (srv:name service)
          (srv:prof-name service)))

(define (update-client-choices-ap)
  (let ((clients (hash->sorted-list client-choices)))
    (send add-appointment-dialog-client-list clear)
    (for-each (λ (client)
                (send add-appointment-dialog-client-list
                      append (client->choice-item client)))
              clients)))

(define (update-client-choices-filter)
  (let ((clients (hash->sorted-list client-choices)))
    (send filter-appointment-client-list clear)
    (for-each (λ (client)
                (send filter-appointment-client-list
                      append (client->choice-item client)))
              clients)))

(define (update-service-choices-ap)
  (let ((services (hash->sorted-list service-choices)))
    (send add-appointment-dialog-service-list clear)
    (for-each (λ (service)
                (send add-appointment-dialog-service-list
                      append (service->choice-item service)))
              services)))

(define (update-service-choices-filter)
  (let ((services (hash->sorted-list service-choices)))
    (send filter-appointment-service-list clear)
    (for-each (λ (service)
                (send filter-appointment-service-list
                      append (service->choice-item service)))
              services)))

(define (index->client n)
  (hash-ref client-choices n))

(define (index->service n)
  (hash-ref service-choices n))

(define (index->appointment n)
  (hash-ref appointment-hash n))

(define (empty-string? str)
  (string=? "" str))

(define (valid-number? str)
  (let ((n (string->number str 10 'number-or-false)))
    (and n (>= n 0))))

(define (valid-date? year month day hour minute (past? #f))
  (letrec ((between?
            (λ (n x y)
              (and (>= n x)
                   (<= n y)))))
    (and (andmap valid-number? (list year month day hour minute))
         (between? (string->number month) 1 12)
         (between? (string->number day) 1 31)
         (between? (string->number hour) 0 23)
         (between? (string->number minute) 0 59)
         (if (not past?)
             (not (past-date? (string->number year)
                              (string->number month)
                              (string->number day)
                              (string->number hour)
                              (string->number minute)))
             #t))))

(define (valid-client-entry?)
  ;; tbh we can only check a. that all fields are non-empty and
  ;; b. that the id and phone fields are actually non-negative numbers
  (let ((name (send clients-add-name-field get-value))
        (id (send clients-add-id-field get-value))
        (phone (send clients-add-phone-field get-value)))
    (and (andmap non-empty-string? (list id phone name))
         (andmap valid-number? (list id phone)))))

(define (valid-service-entry?)
  (let ((service (send serv-add-name-field get-value))
        (profname (send serv-add-profname-field get-value)))
    (andmap non-empty-string? (list service profname))))


(define (valid-appointment-entry?)
  ;;; b o i
  (let ((day (send add-appointment-dialog-day-tf get-value))
        (month (send add-appointment-dialog-month-tf get-value))
        (year (send add-appointment-dialog-year-tf get-value))
        (hour (send add-appointment-dialog-hour-tf get-value))
        (minute (send add-appointment-dialog-minute-tf get-value))
        (payment (send add-appointment-dialog-payment get-value))
        (total (send add-appointment-dialog-total get-value))
        (client (send add-appointment-dialog-client-list get-selection))
        (service (send add-appointment-dialog-service-list get-selection)))
    (and (andmap non-empty-string? (list day month year hour minute total))
         (valid-date? year month day hour minute)
         (or (empty-string? payment) (valid-number? payment))
         client
         service)))

(define (valid-appointment-filter?)
  (let ((from-day (send filter-appointment-interval-from-day-tf get-value))
        (from-month (send filter-appointment-interval-from-month-tf get-value))
        (from-year (send filter-appointment-interval-from-year-tf get-value))
        (to-day (send filter-appointment-interval-to-day-tf get-value))
        (to-month (send filter-appointment-interval-to-month-tf get-value))
        (to-year (send filter-appointment-interval-to-year-tf get-value)))
    (and (or (send filter-appointment-date-check get-value)
             (send filter-appointment-client-check get-value)
             (send filter-appointment-service-check get-value))
         (if (send filter-appointment-date-check get-value)
             (if (= 0 (send filter-appointment-interval-radio-box get-selection))
                 (and (valid-date? from-year from-month from-day "0" "0" #t)
                      (valid-date? to-year to-month to-day "0" "0" #t))
                 #t)
             #t)
         (if (send filter-appointment-client-check get-value)
             (send filter-appointment-client-list get-selection) #t)
         (if (send filter-appointment-service-check get-value)
             (send filter-appointment-service-list get-selection) #t))))

(define (client-hash->table h)
  (letrec ((build
            (λ (ids names phones clients)
              (cond ((null? clients) (map reverse (list ids names phones)))
                    (else (build (cons (number->string (cln:id (car clients))) ids)
                                 (cons (cln:name (car clients)) names)
                                 (cons (cln:phone-number (car clients)) phones)
                                 (cdr clients)))))))
    (build '() '() '() (hash->sorted-list h))))

(define (service-hash->table h)
  (letrec ((build
            (λ (snames pnames servs)
              (cond ((null? servs) (map reverse (list snames pnames)))
                    (else (build (cons (srv:name (car servs)) snames)
                                 (cons (srv:prof-name (car servs)) pnames)
                                 (cdr servs)))))))
    (build '() '() (hash->sorted-list h))))

(define (apts-hash->table h)
  (letrec ((build
            (λ (dates times servs names phones pays notes apts)
              (cond ((null? apts) (map reverse (list dates times servs names phones pays notes)))
                    (else (build
                           (cons (time->date-string (apt:date (car apts))) dates)
                           (cons (time->time-string (apt:date (car apts))) times)
                           (cons (srv:name (apt:service (car apts))) servs)
                           (cons (cln:name (apt:client (car apts))) names)
                           (cons (cln:phone-number (apt:client (car apts))) phones)
                           (cons (format "$~a" (number->string
                                                (pay:paid-amount (apt:payment (car apts)))))
                                 pays)
                           (cons (apt:notes (car apts)) notes)
                           (cdr apts)))))))
    (build '() '() '() '() '() '() '()
           (filter current-filter (hash->sorted-list h)))))

(define (dispatch-client)
  (let ((name (send clients-add-name-field get-value))
        (id (string->number (send clients-add-id-field get-value)))
        (phone (send clients-add-phone-field get-value)))
    (thread-send
     central-thread
     (dis:make 'add
               (msg:make 'add (cln:make id name phone))))))

(define (clear-client-fields)
  (for-each (λ (f) (send f set-value ""))
            (list clients-add-id-field
                  clients-add-name-field
                  clients-add-phone-field)))

(define (dispatch-service)
  (let ((service (send serv-add-name-field get-value))
        (profname (send serv-add-profname-field get-value)))
    (thread-send
     central-thread
     (dis:make 'add
               (msg:make 'add (srv:make 0 service profname))))))

(define (clear-service-fields)
  (for-each (λ (f) (send f set-value ""))
            (list serv-add-name-field
                  serv-add-profname-field)))

(define (dispatch-appointment)
  (let ((minute (string->number (send add-appointment-dialog-minute-tf get-value)))
        (hour (string->number (send add-appointment-dialog-hour-tf get-value)))
        (day (string->number (send add-appointment-dialog-day-tf get-value)))
        (month (string->number (send add-appointment-dialog-month-tf get-value)))
        (year (string->number (send add-appointment-dialog-year-tf get-value)))
        (service (index->service (send add-appointment-dialog-service-list get-selection)))
        (client (index->client (send add-appointment-dialog-client-list get-selection)))
        (payment (string->number (send add-appointment-dialog-payment get-value)))
        (total (string->number (send add-appointment-dialog-total get-value)))
        (notes (send add-appointment-dialog-notes get-value)))
    (thread-send
     central-thread
     (dis:make 'add
               (msg:make 'add
                         (apt:make 0 (destructured-date->time year month day hour minute)
                                   service (pay:make payment total) client notes))))))

(define (clear-appointment-fields)
  (for-each (λ (f) (send f set-value ""))
            (list add-appointment-dialog-minute-tf
                  add-appointment-dialog-hour-tf
                  add-appointment-dialog-day-tf
                  add-appointment-dialog-month-tf
                  add-appointment-dialog-year-tf
                  add-appointment-dialog-notes
                  add-appointment-dialog-payment
                  add-appointment-dialog-total)))

(define (dispatch-clients-deletion)
  (let ((clients (map index->client (send clients-list get-selections))))
    (for-each (λ (client)
                (thread-send
                 central-thread
                 (dis:make 'diff
                           (msg:make 'diff client))))
              clients)))

(define (dispatch-services-deletion)
  (let ((services (map index->service (send serv-list get-selections))))
    (for-each (λ (service)
                (thread-send
                 central-thread
                 (dis:make 'diff
                           (msg:make 'diff service))))
              services)))

(define (build-filter-predicate)
  (let ((date-p (if (send filter-appointment-date-check get-value)
                    (if (= 0 (send filter-appointment-interval-radio-box get-selection))
                        (let ((from (destructured-date->time
                                     (string->number (send filter-appointment-interval-from-year-tf get-value))
                                     (string->number (send filter-appointment-interval-from-month-tf get-value))
                                     (string->number (send filter-appointment-interval-from-day-tf get-value))
                                     0 0))
                              (to (destructured-date->time
                                   (string->number (send filter-appointment-interval-to-year-tf get-value))
                                   (string->number (send filter-appointment-interval-to-month-tf get-value))
                                   (string->number (send filter-appointment-interval-to-day-tf get-value))
                                   23 59)))
                          (λ (apt)
                            (let ((date (apt:date apt)))
                              (and (>= date from)
                                   (<= date to)))))
                        (receive (from to) (today-range)
                          (λ (apt)
                            (let ((date (apt:date apt)))
                              (and (>= date from)
                                   (<= date to))))))
                    (λ (x) #t)))
        (client-p (if (send filter-appointment-client-check get-value)
                      (let ((id (cln:id
                                 (index->client (send filter-appointment-client-list
                                                      get-selection)))))
                        (λ (apt) (= (cln:id (apt:client apt))
                                    id)))
                      (λ (x) #t)))
        (service-p (if (send filter-appointment-service-check get-value)
                       (let ((id (srv:id
                                  (index->service (send filter-appointment-service-list
                                                        get-selection)))))
                         (λ (apt) (= (srv:id (apt:service apt))
                                     id)))
                       (λ (x) #t))))
    (λ (apt)
      (and (date-p apt)
           (client-p apt)
           (service-p apt)))))

(define (dispatch-appointment-deletion)
  (let ((apts (map cdr (hash->list appointment-hash))))
    (for-each (λ (apt)
                (thread-send
                 central-thread
                 (dis:make 'diff
                           (msg:make 'diff apt))))
              apts)))

(define (dispatch-selected-appointments-deletion)
  (let ((apts (map index->appointment (send apts-list get-selections))))
    (for-each (λ (apt)
                (thread-send
                 central-thread
                 (dis:make 'diff
                           (msg:make 'diff apt))))
              apts)))

(define (format-appointments)
  (map appointment-format (appointment-list-by-date)))

(define (pretty-print-appointment apt dc)
  (let ((lines (appointment-pretty-format apt)))
    (letrec ((woosh
              (λ (lines line-no width total-height)
                (cond ((null? lines) (send dc set-brush "white" 'transparent)
                                     (send dc draw-rectangle 0 0 (+ width 20) (+ total-height 20)))
                      (else (receive (w h d a) (send dc get-text-extent (car lines))
                              (send dc draw-text (car lines) 10 (+ 10 (* line-no h)))
                              (woosh (cdr lines)
                                     (add1 line-no)
                                     (if (> w width) w width)
                                     (+ total-height h))))))))
      (woosh lines 0 0 0))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; Fear, ye mortal, the dark ocean of widgets beneath this post
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; we need to derive a class just to have a custom close method
;; and I absolutely hate that
(define amser-window%
  (class frame%
    (super-new)
    (define (on-close)
      (thread-send central-thread eof))
    (augment on-close)))

(define tlw (new amser-window%
                 (label "Amser")
                 (min-width 800)
                 (min-height 600)
                 (border 10)
                 (spacing 10)))

(define menu-bar (new menu-bar%
                      (parent tlw)))

(define file-menu (new menu%
                       (parent menu-bar)
                       (label (fetch-text 'menu-file))))

(define file-menu-print-item
  (new menu-item%
       (parent file-menu)
       (label (fetch-text 'menu-file-print))
       (callback (λ (it ev)
                   (let ((apts (cons (header-format) (format-appointments))))
                     (parameterize ((current-ps-setup (get-page-setup-from-user)))
                       (define pdc (new printer-dc% (parent tlw)))
                       (send pdc set-font (make-font #:size 9 #:family 'modern))
                       (send pdc start-doc (fetch-text 'menu-file-print))
                       (send pdc start-page)
                       (for-each (λ (apt n)
                                   (when (= (modulo (add1 n) 30) 0)
                                     (send pdc end-page)
                                     (send pdc start-page))
                                   (receive (w h d a) (send pdc get-text-extent apt)
                                     (send pdc draw-text apt 10 (* n h))))
                                 apts (range (length apts)))
                       (send pdc end-page)
                       (send pdc end-doc)))))))

(define file-menu-pretty-print-item
  (new menu-item%
       (parent file-menu)
       (label (fetch-text 'menu-file-pretty-print))
       (callback (λ (it ev)
                   (when (send apts-list get-selection)
                       (let ((apt (index->appointment (send apts-list get-selection))))
                         (parameterize ((current-ps-setup (get-page-setup-from-user)))
                           (define pdc (new printer-dc% (parent tlw)))
                           (send pdc set-font (make-font #:size 12 #:family 'modern))
                           (send pdc start-doc (fetch-text 'menu-file-print))
                           (send pdc start-page)
                           (pretty-print-appointment apt pdc)
                           (send pdc end-page)
                           (send pdc end-doc))))))))

(define file-menu-separator (new separator-menu-item%
                                 (parent file-menu)))

(define file-menu-quit-item (new menu-item%
                                 (parent file-menu)
                                 (label (fetch-text 'close))
                                 (callback (λ (it ev)
                                             (send tlw show #f)
                                             (thread-send central-thread eof)))))

(define appt-menu (new menu%
                       (parent menu-bar)
                       (label (fetch-text 'menu-appointment))))

(define appt-menu-add-item (new menu-item%
                                (parent appt-menu)
                                (label (fetch-text 'menu-appointment-new))
                                (callback (λ (it ev)
                                            (send add-appointment-dialog show #t)))))

(define appt-menu-filter-item (new menu-item%
                                   (parent appt-menu)
                                   (label (fetch-text 'menu-appointment-filter))
                                   (callback (λ (it ev)
                                               (send filter-appointment-dialog show #t)))))

(define appt-menu-separator-1 (new separator-menu-item%
                                   (parent appt-menu)))

(define appt-menu-delete-item (new menu-item%
                                   (parent appt-menu)
                                   (label (fetch-text 'menu-appointment-delete))
                                   (callback (λ (it ev)
                                               (send delete-appointment-dialog show #t)))))

(define appt-menu-delete-selected
  (new menu-item%
       (parent appt-menu)
       (label (fetch-text 'menu-appointment-delete-selected))
       (callback (λ (it ev)
                   (send delete-appointment-selected-dialog show #t)))))

(define appt-menu-separator-2 (new separator-menu-item%
                                 (parent appt-menu)))

(define appt-menu-delete-filter-item (new menu-item%
                                          (parent appt-menu)
                                          (label (fetch-text 'menu-appointment-delete-filter))
                                          (callback (λ (it ev)
                                                      (restore-filter!)
                                                      (thread-send
                                                       central-thread
                                                       (msg:make 'refresh 'appointments))))))
(define about-menu (new menu%
                        (parent menu-bar)
                        (label (fetch-text 'menu-about))))

(define about-menu-amser-item (new menu-item%
                                   (parent about-menu)
                                   (label "Amser")
                                   (callback (λ (it ev) (send about-frame show #t)))))

(define tabs (new tab-panel%
                  (parent tlw)
                  (choices (list (fetch-text 'appointments)
                                 (fetch-text 'clients)
                                 (fetch-text 'services)))
                  (alignment '(center bottom))
                  (callback (λ (tabp ev)
                              (let ((n (send tabs get-selection)))
                                (cond ((zero? n)
                                       (send tabs
                                             delete-child
                                             (car (send tabs get-children)))
                                       (send tabs add-child apts-panel))
                                      ((= n 1) (send tabs
                                                     delete-child
                                                     (car (send tabs get-children)))
                                               (send tabs add-child clients-panel))
                                      (else (send tabs delete-child
                                                  (car (send tabs get-children)))
                                            (send tabs add-child serv-panel))))))))


(define apts-panel (new panel%
                        (parent tabs)))

(define apts-list (new list-box%
                       (parent apts-panel)
                       (label #f)
                       (style '(multiple column-headers))
                       (choices '())
                       (columns (list (fetch-text 'date)
                                      (fetch-text 'time)
                                      (fetch-text 'service)
                                      (fetch-text 'name)
                                      (fetch-text 'phone)
                                      (fetch-text 'payment)
                                      (fetch-text 'notes)))))

(define clients-panel (new horizontal-panel%
                           (parent tabs)))

(define clients-list-panel (new vertical-panel%
                                (parent clients-panel)))

(define clients-list (new list-box%
                          (parent clients-list-panel)
                          (label #f)
                          (style '(multiple column-headers))
                          (choices '())
                          (columns (list (fetch-text 'id)
                                         (fetch-text 'name)
                                         (fetch-text 'phone)))))

(define clients-remove-button (new button%
                                   (parent clients-list-panel)
                                   (label (fetch-text 'delete-selection))
                                   (callback (λ (b e)
                                               (send clients-remove-dialog show #t)))))

(define clients-remove-dialog
  (new dialog%
       (label (fetch-text 'confirm))
       (border 10)
       (spacing 10)))

(define clients-remove-message
  (new message%
       (parent clients-remove-dialog)
       (label (fetch-text 'client-delete-message))))

(define clients-remove-button-box
  (new horizontal-panel%
       (parent clients-remove-dialog)
       (alignment '(center center))))

(define clients-remove-cancel-button
  (new button%
       (parent clients-remove-button-box)
       (label (fetch-text 'cancel))
       (callback (λ (b e)
                   (send clients-remove-dialog show #f)))))

(define clients-remove-confirm-button
  (new button%
       (parent clients-remove-button-box)
       (label (fetch-text 'confirm))
       (callback (λ (b e)
                   (let ((selections (send clients-list get-selections)))
                     (when (not (null? selections))
                       (dispatch-clients-deletion)))
                   (send clients-remove-dialog show #f)))))


(define clients-add-panel (new vertical-panel%
                               (parent clients-panel)
                               (border 10)
                               (spacing 10)
                               (alignment '(center center))))

(define clients-add-id-field (new text-field%
                                  (parent clients-add-panel)
                                  (label (fetch-text 'id))))

(define clients-add-name-field (new text-field%
                                    (parent clients-add-panel)
                                    (label (fetch-text 'full-name))))

(define clients-add-phone-field (new text-field%
                                     (parent clients-add-panel)
                                     (label (fetch-text 'phone))))

(define clients-add-button (new button%
                                (parent clients-add-panel)
                                (label (fetch-text 'add))
                                (callback
                                 (λ (b e)
                                   (if (valid-client-entry?)
                                       (dispatch-client)
                                       (send clients-add-invalid-dialog show #t))
                                   (clear-client-fields)))))

(define clients-add-invalid-dialog
  (new dialog%
       (label (fetch-text 'error))
       (spacing 10)
       (border 10)))

(define clients-add-invalid-msg
  (new message%
       (parent clients-add-invalid-dialog)
       (label (fetch-text 'invalid-entry))))

(define clients-add-invalid-button
  (new button%
       (parent clients-add-invalid-dialog)
       (label (fetch-text 'close))
       (callback (λ (b e)
                   (send clients-add-invalid-dialog show #f)))))

(define serv-panel (new horizontal-panel%
                        (parent tabs)))

(define serv-list-panel (new vertical-panel%
                             (parent serv-panel)))

(define serv-list (new list-box%
                       (parent serv-list-panel)
                       (label #f)
                       (style '(multiple column-headers))
                       (choices '())
                       (columns (list (fetch-text 'service)
                                      (fetch-text 'prof-name)))))

(define serv-remove-button (new button%
                                (parent serv-list-panel)
                                (label (fetch-text 'delete-selection))
                                (callback (λ (b e)
                                            (send serv-remove-dialog show #t)))))

(define serv-remove-dialog
  (new dialog%
       (label (fetch-text 'confirm))
       (spacing 10)
       (border 10)))

(define serv-remove-message
  (new message%
       (parent serv-remove-dialog)
       (label (fetch-text 'service-delete-message))))

(define serv-remove-button-box
  (new horizontal-panel%
       (parent serv-remove-dialog)
       (alignment '(center center))))

(define serv-remove-cancel-button
  (new button%
       (parent serv-remove-button-box)
       (label (fetch-text 'cancel))
       (callback (λ (b e) (send serv-remove-dialog show #f)))))

(define serv-remove-confirm-button
  (new button%
       (parent serv-remove-button-box)
       (label (fetch-text 'confirm))
       (callback (λ (b e)
                   (let ((selections (send serv-list get-selections)))
                     (when (not (null? selections))
                       (dispatch-services-deletion)))
                   (send serv-remove-dialog show #f)))))


(define serv-add-panel (new vertical-panel%
                            (parent serv-panel)
                            (border 10)
                            (spacing 10)
                            (alignment '(center center))))

(define serv-add-name-field (new text-field%
                                 (parent serv-add-panel)
                                 (label (fetch-text 'service))))

(define serv-add-profname-field (new text-field%
                                     (parent serv-add-panel)
                                     (label (fetch-text 'prof-name))))

(define serv-add-button (new button%
                             (parent serv-add-panel)
                             (label (fetch-text 'add))
                             (callback (λ (b e)
                                         (if (valid-service-entry?)
                                             (dispatch-service)
                                             (send serv-add-invalid-dialog show #t))
                                         (clear-service-fields)))))

(define serv-add-invalid-dialog
  (new dialog%
       (label (fetch-text 'error))
       (spacing 10)
       (border 10)))

(define serv-add-invalid-msg
  (new message%
       (parent serv-add-invalid-dialog)
       (label (fetch-text 'invalid-entry))))

(define serv-add-invalid-button
  (new button%
       (parent serv-add-invalid-dialog)
       (label (fetch-text 'close))
       (callback (λ (b e)
                   (send serv-add-invalid-dialog show #f)))))

(define about-frame (new dialog%
                         (label (fetch-text 'about-amser))
                         (min-width 400)
                         (min-height 300)
                         (border 10)
                         (spacing 10)))

(define about-frame-panel (new vertical-panel%
                               (parent about-frame)
                               (alignment '(center center))))

(define about-icon (new message%
                        (parent about-frame-panel)
                        (label 'app)))

(define about-text-box
  (new vertical-panel%
       (parent about-frame-panel)
       (alignment '(left center))))

(define license-message-1 (new message%
                               (parent about-text-box)
                               (label (fetch-text 'license1))))

(define license-message-2 (new message%
                               (parent about-text-box)
                               (label (fetch-text 'license2))))

(define license-message-3 (new message%
                               (parent about-text-box)
                               (label "© 2020 Ariela Wenner")))

(define close-about-button (new button%
                                (parent about-frame-panel)
                                (label (fetch-text 'close))
                                (callback (λ (btn ev) (send about-frame show #f)))))

(define delete-appointment-dialog
  (new dialog%
       (label (fetch-text 'confirm))
       (spacing 10)
       (border 10)))

(define delete-appointment-message
  (new message%
       (parent delete-appointment-dialog)
       (label (fetch-text 'appointment-delete-message))))

(define delete-appointment-button-box
  (new horizontal-panel%
       (parent delete-appointment-dialog)
       (alignment '(center center))))

(define delete-appointment-cancel-button
  (new button%
       (parent delete-appointment-button-box)
       (label (fetch-text 'cancel))
       (callback (λ (b e)
                   (send delete-appointment-dialog show #f)))))

(define delete-appointment-confirm-button
  (new button%
       (parent delete-appointment-button-box)
       (label (fetch-text 'confirm))
       (callback (λ (b e)
                   (dispatch-appointment-deletion)
                   (restore-filter!)
                   (send delete-appointment-dialog show #f)))))

(define delete-appointment-selected-dialog
  (new dialog%
       (label (fetch-text 'confirm))
       (spacing 10)
       (border 10)))

(define delete-appointment-selected-msg
  (new message%
       (parent delete-appointment-selected-dialog)
       (label (fetch-text 'appointment-delete-selected-message))))

(define delete-appointment-selected-button-box
  (new horizontal-panel%
       (parent delete-appointment-selected-dialog)
       (alignment '(center center))))

(define delete-appointment-selected-cancel-button
  (new button%
       (parent delete-appointment-selected-button-box)
       (label (fetch-text 'cancel))
       (callback (λ (b e) (send delete-appointment-selected-dialog show #f)))))

(define delete-appointment-selected-confirm-button
  (new button%
       (parent delete-appointment-selected-button-box)
       (label (fetch-text 'confirm))
       (callback (λ (b e)
                   (dispatch-selected-appointments-deletion)
                   (send delete-appointment-selected-dialog show #f)))))

(define add-appointment-dialog
  (new dialog%
       (label (fetch-text 'add-appointment))
       (spacing 10)
       (border 10)))

(define add-appointment-panel
  (new vertical-panel%
       (parent add-appointment-dialog)))

(define add-appointment-date-section
  (new horizontal-panel%
       (parent add-appointment-panel)))

(define add-appointment-dialog-day-tf
  (new text-field%
       (parent add-appointment-date-section)
       (label (fetch-text 'day))))

(define add-appointment-dialog-month-tf
  (new text-field%
       (parent add-appointment-date-section)
       (label (fetch-text 'month))))

(define add-appointment-dialog-year-tf
  (new text-field%
       (parent add-appointment-date-section)
       (label (fetch-text 'year))))

(define add-appointment-time-section
  (new horizontal-panel%
       (parent add-appointment-panel)))

(define add-appointment-dialog-hour-tf
  (new text-field%
       (parent add-appointment-time-section)
       (label (fetch-text 'hour))))

(define add-appointment-dialog-minute-tf
  (new text-field%
       (parent add-appointment-time-section)
       (label (fetch-text 'minute))))

(define add-appointment-dialog-list-panel
  (new horizontal-panel%
       (parent add-appointment-panel)
       (alignment '(left center))
       (spacing 10)))

(define add-appointment-dialog-client-list
  (new list-box%
       (parent add-appointment-dialog-list-panel)
       (label "")
       (style '(single column-headers))
       (columns (list (fetch-text 'clients)))
       (choices '())
       (min-width 300)
       (min-height 300)))

(define add-appointment-dialog-service-list
  (new list-box%
       (parent add-appointment-dialog-list-panel)
       (label "")
       (style '(single column-headers))
       (columns (list (fetch-text 'services)))
       (choices '())
       (min-width 300)
       (min-height 300)))

(define add-appointment-dialog-notes
  (new text-field%
       (label (fetch-text 'notes))
       (parent add-appointment-panel)))

(define add-appointment-dialog-payment
  (new text-field%
       (parent add-appointment-panel)
       (label (fetch-text 'payment))))

(define add-appointment-dialog-total
  (new text-field%
       (parent add-appointment-panel)
       (label (fetch-text 'total))))

(define add-appointment-button-box
  (new horizontal-panel%
       (parent add-appointment-dialog)
       (alignment '(center center))))

(define add-appointment-cancel-button
  (new button%
       (parent add-appointment-button-box)
       (label (fetch-text 'cancel))
       (callback (λ (b e)
                   (clear-appointment-fields)
                   (send add-appointment-dialog show #f)))))

(define add-appointment-button
  (new button%
       (parent add-appointment-button-box)
       (label (fetch-text 'add))
       (callback (λ (btn ev)
                   (if (valid-appointment-entry?)
                       (dispatch-appointment)
                       (send invalid-appointment-entry-dialog show #t))
                   (clear-appointment-fields)
                   (send add-appointment-dialog show #f)))))

(define invalid-appointment-entry-dialog
  (new dialog%
       (label (fetch-text 'error))
       (border 10)
       (spacing 10)))

(define invalid-appointment-entry-message
  (new message%
       (parent invalid-appointment-entry-dialog)
       (label (fetch-text 'invalid-entry))))

(define invalid-appointment-entry-dialog-close
  (new button%
       (parent invalid-appointment-entry-dialog)
       (label (fetch-text 'close))
       (callback (λ (b e)
                   (send invalid-appointment-entry-dialog show #f)))))

(define filter-appointment-dialog
  (new dialog%
       (label (fetch-text 'filter-appointment))
       (spacing 15)
       (border 10)))

(define filter-appointment-panel
  (new vertical-panel%
       (parent filter-appointment-dialog)))

(define filter-appointment-date-check
  (new check-box%
       (parent filter-appointment-panel)
       (label (fetch-text 'date))))

(define filter-appointment-interval-group
  (new group-box-panel%
       (parent filter-appointment-panel)
       (label "")))

(define filter-appointment-interval-panel
  (new vertical-panel%
       (parent filter-appointment-interval-group)))

(define filter-appointment-interval-radio-box
  (new radio-box%
       (parent filter-appointment-interval-panel)
       (label "")
       (choices (list (fetch-text 'interval) (fetch-text 'today)))
       (style (list 'horizontal))))

(define filter-appointment-interval-from-box
  (new group-box-panel%
       (parent filter-appointment-interval-panel)
       (label (fetch-text 'from))))

(define filter-appointment-interval-from-section
  (new horizontal-panel%
       (parent filter-appointment-interval-from-box)))

(define filter-appointment-interval-from-day-tf
  (new text-field%
       (parent filter-appointment-interval-from-section)
       (label (fetch-text 'day))))

(define filter-appointment-interval-from-month-tf
  (new text-field%
       (parent filter-appointment-interval-from-section)
       (label (fetch-text 'month))))

(define filter-appointment-interval-from-year-tf
  (new text-field%
       (parent filter-appointment-interval-from-section)
       (label (fetch-text 'year))))

(define filter-appointment-interval-to-box
  (new group-box-panel%
       (parent filter-appointment-interval-panel)
       (label (fetch-text 'to))))

(define filter-appointment-interval-to-section
  (new horizontal-panel%
       (parent filter-appointment-interval-to-box)))

(define filter-appointment-interval-to-day-tf
  (new text-field%
       (parent filter-appointment-interval-to-section)
       (label (fetch-text 'day))))

(define filter-appointment-interval-to-month-tf
  (new text-field%
       (parent filter-appointment-interval-to-section)
       (label (fetch-text 'month))))

(define filter-appointment-interval-to-year-tf
  (new text-field%
       (parent filter-appointment-interval-to-section)
       (label (fetch-text 'year))))

(define filter-appointment-lists-section
  (new horizontal-panel%
       (parent filter-appointment-panel)
       (spacing 10)))

(define filter-appointment-client-section
  (new vertical-panel%
       (parent filter-appointment-lists-section)
       (alignment '(center center))))

(define filter-appointment-client-check
  (new check-box%
       (parent filter-appointment-client-section)
       (label (fetch-text 'client))))

(define filter-appointment-client-list
  (new list-box%
       (parent filter-appointment-client-section)
       (label "")
       (style '(single column-headers))
       (columns (list (fetch-text 'clients)))
       (choices '())
       (min-width 300)
       (min-height 300)))

(define filter-appointment-service-section
  (new vertical-panel%
       (parent filter-appointment-lists-section)
       (alignment '(center center))))

(define filter-appointment-service-check
  (new check-box%
       (parent filter-appointment-service-section)
       (label (fetch-text 'service))))

(define filter-appointment-service-list
  (new list-box%
       (parent filter-appointment-service-section)
       (label "")
       (style '(single column-headers))
       (columns (list (fetch-text 'services)))
       (choices '())
       (min-width 300)
       (min-height 300)))

(define filter-appointment-button-box
  (new horizontal-panel%
       (parent filter-appointment-panel)
       (alignment (list 'center 'center))))

(define filter-appointment-cancel-button
  (new button%
       (parent filter-appointment-button-box)
       (label (fetch-text 'cancel))
       (callback (λ (b e) (send filter-appointment-dialog show #f)))))

(define filter-appointment-confirm-button
  (new button%
       (parent filter-appointment-button-box)
       (label (fetch-text 'confirm))
       (callback (λ (b e)
                   (if (valid-appointment-filter?)
                       (begin
                         (set-filter! (build-filter-predicate))
                         (thread-send central-thread
                                      (msg:make 'refresh 'appointments))
                         (send filter-appointment-dialog show #f))
                       (send invalid-filter-dialog show #t))))))

(define invalid-filter-dialog
  (new dialog%
       (label (fetch-text 'error))
       (border 10)
       (spacing 10)))

(define invalid-filter-message
  (new message%
       (parent invalid-filter-dialog)
       (label (fetch-text 'invalid-filter))))

(define invalid-filter-dialog-close
  (new button%
       (parent invalid-filter-dialog)
       (label (fetch-text 'close))
       (callback (λ (b e)
                   (send invalid-filter-dialog show #f)))))

(define server-disconnect-dialog
  (new dialog%
       (label (fetch-text 'fatal-error))
       (spacing 10)
       (border 10)))

(define server-disconnect-msg
  (new message%
       (parent server-disconnect-dialog)
       (label (fetch-text 'server-disconnect))))

(define server-disconnect-button
  (new button%
       (parent server-disconnect-dialog)
       (label (fetch-text 'accept))
       (callback (λ (b e)
                   (send server-disconnect-dialog show #f)
                   (thread-send central-thread eof)))))

(send tabs delete-child clients-panel)
(send tabs delete-child serv-panel)

;;;; Default column widths
;;; Appointment list
(send apts-list set-column-width 0 100 100 200)
(send apts-list set-column-width 1 75 75 150)
(send apts-list set-column-width 2 150 150 300)
(send apts-list set-column-width 3 150 150 300)
(send apts-list set-column-width 4 100 100 200)
(send apts-list set-column-width 5 50 50 100)

;;; Clients
(send clients-list set-column-width 0 100 100 200)
(send clients-list set-column-width 1 150 150 300)
(send clients-list set-column-width 2 100 100 200)

;;; Services
(send serv-list set-column-width 0 150 150 300)
(send serv-list set-column-width 1 150 150 300)
