#lang racket

(require srfi/8/receive
         (prefix-in db: db)
         (prefix-in dbal: "dbal.common.rkt")
         (prefix-in data:appointment: "data.appointment.rkt")
         (prefix-in data:client: "data.client.rkt")
         (prefix-in data:service: "data.service.rkt")
         (prefix-in data:payment: "data.payment.rkt"))

(provide retrieve-all)

(define (queries)
  '((appointments . "SELECT * FROM appointments")
    (clients . "SELECT * FROM clients")
    (services . "SELECT * FROM services")))

(define (with-clauses)
  '((appointments . " WHERE appointment_id = ?")
    (clients . " WHERE client_id = ?")
    (services . " WHERE service_id = ?")))

(define (retrieve-query name)
  (cdr (assq name (queries))))

(define (retrieve-with-query name)
  (string-append (retrieve-query name)
                 (cdr (assoc name (with-clauses)))))

(define (retrieve-clients)
  (map (λ (row)
         (receive (id name phone) (vector->values row)
           (data:client:make id name phone)))
       (dbal:with-connection
        conn
        (db:query-rows conn
                       (retrieve-query 'clients)))))

(define (retrieve-client-by-id id)
  (receive (id name phone) (dbal:with-connection
                            conn
                            (vector->values
                             (db:query-row conn (retrieve-with-query 'clients) id)))
    (data:client:make id name phone)))

(define (retrieve-services)
  (map (λ (row)
         (receive (id serv prof-name) (vector->values row)
           (data:service:make id serv prof-name)))
       (dbal:with-connection
        conn
        (db:query-rows
         conn
         (retrieve-query 'services)))))

(define (retrieve-service-by-id id)
  (receive (id serv prof-name) (dbal:with-connection
                                conn
                                (vector->values
                                 (db:query-row conn (retrieve-with-query 'services) id)))
    (data:service:make id serv prof-name)))

(define (retrieve-appointments)
  (map (λ (row)
         (receive (id date cid sid paid total notes) (vector->values row)
           (data:appointment:make id date
                                  (retrieve-service-by-id sid)
                                  (data:payment:make paid total)
                                  (retrieve-client-by-id cid)
                                  notes)))
       (dbal:with-connection
        conn
        (db:query-rows conn (retrieve-query 'appointments)))))

(define (retrieve-all type)
  (cond ((eq? type 'appointments) (retrieve-appointments))
        ((eq? type 'clients) (retrieve-clients))
        ((eq? type 'services) (retrieve-services))
        (else (error "Attepted to retrieve unknown structure. Aborting."))))
