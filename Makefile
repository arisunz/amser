
NODE_MAIN = src/node.main.rkt
SERVER_MAIN = src/server.main.rkt

all: node server

node:
	raco exe --gui -o amser-node $(NODE_MAIN)

server:
	raco exe --gui -o amser-server $(SERVER_MAIN)
