# Amser
Simple appointment manager with networking capabilities intended for small to medium businesses.

# Requirements
A recent version of Racket, 7.2 or superior is recommended.

# Usage
Once cloned, either 1. run `make` in the project root or 2. load `src/node.main.rkt` and/or `src/server.main.racket` into DrRacket, and build the executables out of those. You could also run `racket src/node.main.rkt` or `racket/server.main.rkt`, but building executables is highly recommended.

With the default configuration, you will be able to run `node` in local mode with no further configuration required.

To configure Amser, edit the `amser.cfg` file that will be created automatically in `$HOME/.racket/amser` in Linux/Mac OS or `%APPDATA%/Racket/amser` on Windows. The configuration file consists of a list containing key-value pairs. Currently supported configs of interest are `language` and `mode`. Do **NOT** edit the configuration file while Amser is running.

# Modes
By default, Amser will run in `local mode`, in which a single node controls its own database. Changing the `mode` to `net` in the configuration file will toggle networked mode, in which a `server` must be running, managing access to a shared database to multiple `nodes` in the local network.

Note that closing the server will forcibly close all connected nodes immediately (with a nice warning, but still).

# License
© Ariela Wenner - 2020

Code is licenced under the GNU GPL version 3 or newer. See [COPYING](COPYING.md) for details.
